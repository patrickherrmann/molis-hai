module Lib where

import Protolude hiding (State)
import Prelude (tail, last, (!!))
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Sequence ((|>))
import qualified Data.Sequence as Seq
import qualified Data.PQueue.Min as Q
import qualified Data.ByteString as BS
import Crypto.Random

type StateMap t a = Map [t] a
type Freqs t = Map t Int
type Probs t = Map t (Ratio Int)
type BitString = [Bool]

data Huff t = Branch (Huff t) (Huff t) | Leaf t
  deriving (Show)

data HuffNode t = HN (Huff t) (Ratio Int)

instance Eq (HuffNode t) where
  (HN _ a) == (HN _ b) = a == b

instance Ord (HuffNode t) where
  compare (HN _ a) (HN _ b) = compare a b

type StateMachine t = StateMap t (Huff t)

transitions :: [t] -> Int -> [([t], t)]
transitions cs size = zipWith const (map trans rotations) cs
  where
    rotations = tails (cycle cs)
    trans rot = (take size rot, rot !! size)

recordTransitions :: Ord t => [([t], t)] -> StateMap t (Freqs t)
recordTransitions = foldl' (flip recordTransition) Map.empty

recordTransition :: Ord t => ([t], t) -> StateMap t (Freqs t) -> StateMap t (Freqs t)
recordTransition (from, to) stateMap = Map.alter f from stateMap
  where
    f Nothing = Just $ Map.singleton to 1
    f (Just freqs) = Just $ Map.alter g to freqs
    g Nothing = Just 1
    g (Just c) = Just $ c + 1

normalize :: Freqs t -> Probs t
normalize freqs = (% total) <$> freqs
  where
    total = sum freqs

buildHuff :: Probs t -> Huff t
buildHuff probs = go $ Q.fromList nodes
  where
    nodes = map (\(s, r) -> HN (Leaf s) r) $ Map.assocs probs
    peek2 q = do
      (a, q') <- Q.minView q
      (b, q'') <- Q.minView q'
      pure (a, b, q'')
    go q = case peek2 q of
      Nothing -> case Q.findMin q of
        HN s _ -> s
      Just (HN sa ra, HN sb rb, q'') -> go $ Q.insert (HN (Branch sa sb) (ra + rb)) q''

buildStateMachine :: Ord t => [t] -> Int -> (StateMachine t, [t])
buildStateMachine seed size = (buildHuff . normalize <$> freqs, highestOutDegree)
  where
    freqs = recordTransitions $ transitions seed size
    highestOutDegree = fst $ maximumBy (comparing (Map.size . snd)) $ Map.assocs freqs

generate :: Ord t => StateMachine t -> [t] -> BitString -> [t]
generate sm s0 bits = toList $ go s0 (sm Map.! s0) bits Seq.empty
  where
    go _ (Leaf c) [] acc = acc |> c
    go s (Leaf c) bs acc = go s' t' bs (acc |> c)
      where
        s' = (tail s <> [c])
        t' = sm Map.! s'
    go s (Branch _ f) bs@[] acc = go s f bs acc
    go s (Branch t f) (b:bs) acc = go s (if b then t else f) bs acc

getEntropy :: Int -> IO BitString
getEntropy bits = do
  let byteCount = (bits `div` 8) + (if bits `rem` 8 == 0 then 0 else 1)
  bytes <- getRandomBytes byteCount
  let toBits byte = testBit byte <$> [0..7]
  pure . take bits $ BS.unpack bytes >>= toBits

