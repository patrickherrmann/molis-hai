module Main where

import Protolude
import Prelude (words, unwords)
import Lib
import Options
import Data.Char

main :: IO ()
main = do
  opts <- parseOpts
  rawSource <- readRawSource (sourceOpts opts)
  bitStrings <- getBitstrings (bitSourceOpts opts)
  let source = preprocess (preprocessOpts opts) (toS rawSource)
  let pws = generatePasswords bitStrings (modelOpts opts) source
  traverse_ putStrLn pws

generatePasswords :: [BitString] -> Model -> [Char] -> [[Char]]
generatePasswords bss = \case
    Letters size -> pwds size
    Words size -> map unwords . pwds size . words
  where
    pwds :: Ord t => Int -> [t] -> [[t]]
    pwds size ts = let (sm, s0) = buildStateMachine ts size in generate sm s0 <$> bss

createPasswords :: Ord t => [t] -> Int -> [BitString] -> [[t]]
createPasswords ts size bss = generate sm s0 <$> bss
  where
    (sm, s0) = buildStateMachine ts size

readRawSource :: Source -> IO Text
readRawSource = \case
  StdIn -> getContents
  File fn -> readFile (toS fn)

getBitstrings :: BitSource -> IO [BitString]
getBitstrings = \case
  Custom bss -> pure $ map (== '1') . toS <$> bss
  Gen bits count -> replicateM count $ getEntropy bits

preprocess :: Preprocess -> [Char] -> [Char]
preprocess opts = collapseWhitespace . process
  where
    collapseWhitespace = unwords . words
    process = processPunctuation . processCase
    processPunctuation = if (keepPunctuation opts)
      then identity
      else filter (not . isPunctuation)
    processCase = if lowercase opts
      then map toLower
      else identity
