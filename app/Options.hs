module Options where

import Protolude hiding (option)
import Options.Applicative

parseOpts :: IO MolisHaiOptions
parseOpts = execParser opts

opts :: ParserInfo MolisHaiOptions
opts = info (molisHaiOptions <**> helper)
  $ fullDesc
  <> header "Molis Hai - Generate secure passwords from a Markov model of a source text"

data MolisHaiOptions = MolisHaiOptions
  { sourceOpts :: Source
  , bitSourceOpts :: BitSource
  , modelOpts :: Model
  , preprocessOpts :: Preprocess
  }
data Source = StdIn | File Text
data BitSource = Custom [Text] | Gen Int Int
data Model = Letters Int | Words Int
data Preprocess = Preprocess
  { keepPunctuation :: Bool
  , lowercase :: Bool
  }

molisHaiOptions :: Parser MolisHaiOptions
molisHaiOptions = MolisHaiOptions
    <$> source
    <*> bitSource
    <*> model
    <*> preprocess
  where
    source = stdin <|> file
    stdin = flag' StdIn
      $  long "stdin"
      <> help "Read source material from standard input."
    file = fmap File $ strOption
      $  long "file"
      <> short 'f'
      <> metavar "FILENAME"
      <> help "Read source material from FILENAME."
    bitSource = custom <|> gen
    custom = fmap Custom $ some $ strOption
      $  long "custom-bits"
      <> metavar "BITSTRING"
      <> help "Provide your own bits instead of generating them randomly."
    gen = Gen <$> bits <*> count
    bits = option auto
      $  long "bits"
      <> short 'b'
      <> metavar "BITS"
      <> value 40
      <> showDefault
      <> help "How many bits of entropy to generate."
    count = option auto
      $  long "count"
      <> short 'c'
      <> metavar "C"
      <> value 1
      <> showDefault
      <> help "How many passwords to generate, so a favorite can be chosen. Note that generating C passwords reduces strength by log2(C) bits of entropy."
    model = words <|> letters
    letters = fmap Letters $ option auto
      $  long "letters"
      <> short 'l'
      <> metavar "N"
      <> value 4
      <> showDefault
      <> help "Build states from N consecutive letters"
    words = fmap Words $ option auto
      $  long "words"
      <> short 'w'
      <> metavar "N"
      <> value 1
      <> showDefault
      <> help "Build states from N consecutive words"
    preprocess = Preprocess <$> punc <*> lower
    punc = switch
      $  long "keep-punctuation"
      <> short 'p'
      <> help "Keep punctuation from source text."
    lower = switch
      $  long "lowercase"
      <> help "Transform the source text to lowercase"
