# Molis Hai

An implementation of the password generation method described in [a paper](https://arxiv.org/abs/1502.07786) by John Clements. Molis Hai works by first building a Markov model from a source text of your choice. It then interprets a random series of bits as huffman coded state transitions, 'decompressing' them into text that appears similar to the source text.

The Markov model can build states out of sequences of N letters (`-l N`) or sequences of N words (`-w N`). Longer sequences will produce longer passwords but will more closely resemble the source text. The amount of entropy in the password is unchanged. You can adjust the desired number of bits of entropy in the generated password (`-b`).

If you generate multiple passwords to choose from (`-c`), the entropy of the chosen password is decreased (because you traded some randomness for choice). For example, if you generate 8 32-bit passwords and choose one, that password has a strength of 29 bits. Rerunning the command to see more choices will similarly reduce entropy.

Any plain text can be used as a model. Project Gutenburg is a good source for plaintext classic novels, e.g.:

```
wget http://www.gutenberg.org/cache/epub/174/pg174.txt -O dorian-gray.txt
```

## Examples

```
molis-hai -f dorian-gray.txt
```

Generate a 40-bit password from The Picture of Dorian Gray. By default, each state consists of a single word.

* "man who had all the most of sins"
* "previous to the burden of which we could"
* "hand over to consult his feet I see"
* "freedom of a delightful dream a theory"

States of two words or more will produce very long passwords, although larger source texts keep the size smaller.

Using the works of shakespeare, `-w 2` can generate some reasonably small passwords:

* "names of those virtues vacant I fear I was sent to her such"
* "freedom of my own searching eyes shall find yourself to make his"
* "evening and no more quoth he say the citizens"

---

```
molis-hai --stdin --custom-bits 00000000 --custom-bits 11111111
```

Build a Markov model from standard input, then use it to create a password from the provided bits. You may provide the `--custom-bits` option more than once to generate multiple passwords from supplied bits.

---

```
molis-hai -f dorian-gray.txt -l 2 -p -c 8 -b 59
```

Generate 8 compact 59-bit passwords. With only 2 letters in each state, the passwords are closer to gibberish. Punctuation is kept. The chosen password has an entropy of 56 bits, since choosing from a set of 8 removes 3 bits of entropy.

* "20% offouldeoppery tw,"
* "ont lippereakfibui"

Try using `-l 3` to see longer passwords that are slightly more readable:

* "marribe hiddles felocks"
* "in and, to bach ducest ne"

Or `-l 4`:

* "his been! All run awkward on, which its"
* "Sobies off, on the felt thought you thin"

## Installation

To build the code and install the executable at `~/.local/bin`, run `stack install`.

## Completion

Bash completion is available:

```
source <(molis-hai --bash-completion-script `which molis-hai`)
```
